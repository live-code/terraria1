import { Component } from '@angular/core';

@Component({
  selector: 'te-root',
  template: `
    <button routerLink="home" routerLinkActive="bg-warning">home</button>
    <button routerLink="uikit" routerLinkActive="bg-warning">uikit</button>
    <button routerLink="login" routerLinkActive="bg-warning">login</button>
    <button routerLink="users" teMyRouterLinkActive="bg-warning">users</button>
    <button routerLink="uikit-directives" teMyRouterLinkActive="bg-warning">directives</button>
    
    <div class="container mt-4">
      <router-outlet></router-outlet>
    </div>
    
  `,
  styles: [`
  `]
})
export class AppComponent {
}
