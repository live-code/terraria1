import { AfterViewInit, Component, HostBinding, Input, OnInit } from '@angular/core';
import { RowComponent } from './row.component';

@Component({
  selector: 'te-col',
  template: `
      <ng-content></ng-content>
  `,
})
export class ColComponent {
  @HostBinding() get class() {
    return 'col-' + this.row.mq;
  }
  constructor(private row: RowComponent) {}
}
