import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'te-row',
  template: `
    <div class="row">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
export class RowComponent {
  @Input() mq: 'sm' | 'md' | 'lg' = 'sm'
}
