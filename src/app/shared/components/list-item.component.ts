import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'te-list-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <li 
        (click)="open = !open">
      {{getTitle(item)}}

      <div class="pull-right">
        <i [class]="icon"
           (click)="iconClickHandler($event, item)"></i>
      </div>
      <div *ngIf="open">
        lorem....
      </div>
    </li>
  `,
  styles: [
  ]
})
export class ListItemComponent<T> {
  @Input() item!: T
  @Input() icon: string | undefined;
  @Input() labelField: string = 'title'
  @Output() iconClick = new EventEmitter<T>()

  open = false;

  getTitle<T>(obj: { [key: string]: any}) {
    return obj[this.labelField]
  }

  iconClickHandler(e: MouseEvent, item: T) {
    e.stopPropagation();
    this.iconClick.emit(item)
  }
}
