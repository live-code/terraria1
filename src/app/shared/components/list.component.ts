import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ThemeService } from '../../core/theme.service';

@Component({
  selector: 'te-list',
  template: `
    <h1>Lista</h1>
    <te-list-item
      *ngFor="let item of items"
      [item]="item"
      [icon]="icon"
      [labelField]="labelField"
      (iconClick)="iconClick.emit($event)"
    ></te-list-item>
    
  `,
})
export class ListComponent<T, K extends keyof T>  {
  @Input() items: T[] | null = [];
  @Input() icon: string | undefined;
  @Input() theme: string | undefined;
  @Input() labelField: string = 'title'
  @Output() iconClick = new EventEmitter<T>()


  render() {
    console.log('render list', this.items)
  }

}

