import { AfterContentInit, ChangeDetectionStrategy, Component, ContentChildren, QueryList } from '@angular/core';
import { GroupComponent } from './group.component';
import { map, Subject, Subscription, take, takeUntil } from 'rxjs';

@Component({
  selector: 'te-accordion',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    
    <div style="border: 3px solid blue; padding: 10px">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
export class AccordionComponent implements AfterContentInit {
  @ContentChildren(GroupComponent) groups!: QueryList<GroupComponent>;
  destroy$ = new Subject();

  ngAfterContentInit(): void {
    const groups = this.groups.toArray()
    if(!groups.length) {
      throw new Error('accordion cannot be empty')
    }

    groups[0].isOpen = true
    groups.forEach(g => {
      g.headerClick
        .pipe(
          takeUntil(this.destroy$)
        )
      .subscribe(() => {
        this.openGroup(g);
      })
    })
  }

  openGroup(groupToOpen: GroupComponent) {
    this.groups.forEach(g => {
     g.isOpen = false;
    })
    groupToOpen.isOpen = true;
  }

  ngOnDestroy() {
    this.destroy$.next(null);
    this.destroy$.complete();
  }
}
