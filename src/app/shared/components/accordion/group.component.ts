import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'te-group',
  template: `
    <div class="card">
      <div 
        class="card-header bg-dark text-white"
        (click)="headerClick.emit()"
      >{{title}}</div>
      <div class="card-body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class GroupComponent {
  @Input() title: string = ''
  @Input() isOpen = false;
  @Output() headerClick = new EventEmitter()

}
