import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { PanelService } from './panel.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'te-panel',
 //  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="card" [ngClass]="{'mb-3': marginBottom}">
      
      <div 
        class="card-header" 
        [ngClass]="headerCls" 
        (click)="toggle()">
        {{title}} - {{panelService.value}}
        <div class="pull-right" *ngIf="icon" 
             (click)="iconClickHandler($event)">
          <i [class]="icon"></i>
        </div>
      </div>
      
      <div class="card-body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  providers: [
    PanelService,
  ]
})
export class PanelComponent {
  @Input() title: string | number = 'widget';
  @Input() icon: string | null = null;
  @Input() headerCls: string = ''
  @Input() marginBottom = true;
  @Input() isOpen = true;
  @Output() iconClick = new EventEmitter();
  @Output() isOpenChange = new EventEmitter();

  constructor(public panelService: PanelService) {

  }

  iconClickHandler(e: MouseEvent) {
    this.panelService.toggle()
    e.stopPropagation()
    this.iconClick.emit()
  }

  toggle() {
    this.isOpen = !this.isOpen;
    this.isOpenChange.emit(this.isOpen);
  }


  render() {
    console.log('render panel')
  }
}
