import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root'})
export class PanelService {
  value = true;
  toggle() {
    this.value = !this.value;
  }
}
