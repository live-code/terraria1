import { Component, ElementRef, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import * as HighCharts from 'highcharts';

@Component({
  selector: 'te-chart-tempeture',
  template: `
    <div #chart style="width:100%; height:400px;"></div>
  `,
})
export class ChartTempetureComponent  {
  @ViewChild('chart', { static: true }) chart!: ElementRef<HTMLElement>
  @Input() temperatures: number[] = [];
  @Input() type: 'bar' | 'line' = 'bar';
  @Output() itemClick = new EventEmitter<number>();

  chartInstance!: HighCharts.Chart;

  ngOnInit() {
    if (!this.chartInstance) {
      this.initChart()
      this.update();
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['temperatures']) {
      if (changes['temperatures'].firstChange) {
        this.initChart()
      } else {
        this.update();
      }
    }
    if (changes['type']) {
      this.update();
    }
  }

  initChart() {
    const options = setOptions(this.temperatures, this.type, this.doItemClick.bind(this) )
    this.chartInstance = HighCharts.chart(this.chart.nativeElement, options);
  }
  update() {
    const options = setOptions(this.temperatures, this.type, this.doItemClick.bind(this))
    this.chartInstance.update(options);
  }
  doItemClick(index: number) {
    this.itemClick.emit(this.temperatures[index])
  }

}

const setOptions: any = (
  temps: number[],
  type: 'bar' | 'line',
  fn: any
) => ({
  chart: {
    type: type,
  },
  title: {
    text: 'Fruit Consumption'
  },
  xAxis: {
    categories: ['LUN', 'MAR', 'MER']
  },
  yAxis: {
    title: {
      text: 'temperatures'
    }
  },
  series: [{
    name: 'Temp',
    data: temps
  }],
  plotOptions: {
    series: {
      cursor: 'pointer',
      point: {
        events: {
          click: function () {
            fn((this as any).index)
          }
        }
      }
    }
  },

})


