|----------------|
| PLUGIN "TOAST" |
|----------------|


Il plugin toast permette di visualizzare dei messaggi sovrapposti alla mappa quando vengono eseguite determinate azioni o si verificano certi eventi.

Sono disponibili quattro tipi di messaggi diversi:

|Tipo		 |Nome	 |Colore   |class css                    |
|------------|-------|---------|-----------------------------|
|successo 	 |success|verde    |leaflet-toast-message-success|
|attenzione  |warn	 |arancione|leaflet-toast-message-warn   |
|errore 	 |error	 |rosso    |leaflet-toast-message-error  |
|informazione|info	 |blu      |leaflet-toast-message-content|


Il plugin toast è configurabile con le seguenti opzioni:

|Nome		 |Default	 					|Funzione   																																							|
|------------|------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|position 	 |'bottomright'    				|imposta la posizione del widget sulla mappa ('bottomright', 'bottomleft', 'topleft', 'topright') 																		|
|summary     |true	     					|mostra il titolo del messaggio (true, false)																															|
|detail 	 |true	     					|mostra il testo del messaggio (true, false)   																															|
|life        |4000	     					|tempo (in millisecondi) di attesa prima della chiusura automatica del widget																							|
|sticky 	 |true    						|se 'true', il messaggio rimane visibile fino alla chiusura manuale. Se 'false', il messaggio viene automaticamente chiuso dopo il tempo definito nella proprietà 'life'|
|closable    |true	     					|se 'true', sul messaggio viene mostrata un icona per chiudere il messaggio manualmente.																				|
|successIcon |'fal fa-check-circle'	     	|icona successo (https://fontawesome.com/)   																															|
|warnIcon    |'fal fa-exclamation-triangle'	|icona attenzione (https://fontawesome.com/)    																														|
|errorIcon   |'fal fa-times'	     		|icona errore (https://fontawesome.com/)   																																|
|infoIcon    |'fal fa-info-circle'	     	|icona informazione (https://fontawesome.com/)  																 														   |


Per l'import del plugin, usare la direttiva:
<ui:include src="/leaflet/import-plugin-toast.xhtml" />

La classe del plugin è:
L.Control.Toast

Sono disponibili quattro metodi, ognuno per una determina severity. I metodi ricevono in ingresso un oggetto composto da title e text. Esempio:
{title:'Titolo successo', text:'Testo successo'}

Esempi di chiamata:
- toasts.success({title:'Titolo successo', text:'Testo successo'});
- toasts.warn({title:'Titolo attenzione', text:'Testo attenzione'});
- toasts.error({title:'Titolo errore', text:'Testo errore'});
- toasts.info({title:'Titolo informazione', text:'Testo informazione'});

