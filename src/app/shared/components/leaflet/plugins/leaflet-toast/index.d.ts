// Type definitions for Leaflet.markercluster 1.4
// Project: https://github.com/Leaflet/Leaflet.markercluster
// Definitions by: Robert Imig <https://github.com/rimig>, Nenad Filipovic <https://github.com/nenadfilipovic>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.3

import * as L from 'leaflet';
import { Control } from 'leaflet';

declare module 'leaflet' {
    class Toast extends Control {
        /*
        * Recursively retrieve all child markers of this cluster.
        */
        success(obj: any): any;

        /*
        * Returns the count of how many child markers we have.
        */
        getChildCount(): number;

    }

    interface MarkerClusterGroupOptions extends LayerOptions {
        /*
        * When you mouse over a cluster it shows the bounds of its markers.
        */
        showCoverageOnHover?: boolean | undefined;
    }
/*
    class MarkerClusterGroup extends FeatureGroup {

    }*/

    /*
    * Create a marker cluster group, optionally given marker cluster group options.
    */
   function toast(options?: any): any;

}
