import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeafletComponent } from './leaflet.component';



@NgModule({
  declarations: [
    LeafletComponent
  ],
  exports: [
    LeafletComponent
  ],
  imports: [
    CommonModule
  ]
})
export class LeafletModule { }
