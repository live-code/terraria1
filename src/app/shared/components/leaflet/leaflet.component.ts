import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import * as L from 'leaflet';
import { LatLngExpression } from 'leaflet';
import 'leaflet.markercluster';
import './plugins/leaflet-toast'
export type Coords = [number, number];


@Component({
  selector: 'te-leaflet',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div #host class="map"></div>
  `,
  styleUrls: ['./leaflet.component.css', './plugins/leaflet-toast/leaflet-toast.css']
})
export class LeafletComponent {
  @ViewChild('host', { static: true }) host!: ElementRef<HTMLDivElement>;
  @Input() zoom: number = 5;
  @Input() coords: LatLngExpression | null | undefined = null;
  @Input() markers: LatLngExpression[] = [];
  map!: L.Map;

  ngOnInit() {
    if (!this.map) {
      this.init();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['coords'] && changes['coords']?.firstChange) {
      this.init();
    }
    if (changes['coords'] && !changes['coords']?.firstChange) {
      if (this.coords) {
        this.map.flyTo(this.coords)
      }
    }
    if (changes['zoom']) {
      this.map.setZoom(this.zoom)
    }
    if (changes['markers']) {
      this.setMarkers()
    }
  }

  init(): void {
    if (this.coords) {
     this.map = L.map(this.host.nativeElement).setView(this.coords, this.zoom);
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      }).addTo(this.map);
      const toast = L.toast({})
      toast.addTo(this.map)

      toast.success({title: 'bla bla'})
    }
  }

  private setMarkers() {

    const clusterMarker = L.markerClusterGroup();

    this.markers.forEach(m => {
      const marker = L.marker(m)
        .bindPopup(`Clicked to , ${m.toString()}`)
        .openPopup();
      clusterMarker.addLayer(marker);
    })

    this.map.addLayer(clusterMarker);
    /*const bounds = new L.LatLngBounds(this.markers);
    this.map.fitBounds(bounds);
    this.map.invalidateSize();*/

  }
}


