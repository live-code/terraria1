import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'te-weather',
  template: `
    <h1>{{city}}</h1>
    <pre *ngIf="meteo">{{meteo.main.temp}}</pre>
  `,
})
export class WeatherComponent implements OnChanges {
  @Input() city: string | null = null;
  @Input() unit: 'imperial' | 'metric' = 'metric'
  meteo: any | null = null;
  timer: any;

  constructor(private http: HttpClient) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.city) {
      this.http.get<any>(`http://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=${this.unit}&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .subscribe(res => {
          this.meteo = res;
        })
    }
  }


  ngOnDestroy() {
    console.log('destroy')

  }
}
