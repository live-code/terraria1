import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherComponent } from './components/weather.component';
import { ChartComponent } from './components/chart.component';
import { PanelModule } from './components/panel/panel.module';
import { ListComponent } from './components/list.component';
import { ListItemComponent } from './components/list-item.component';
import { AccordionComponent } from './components/accordion/accordion.component';
import { GroupComponent } from './components/accordion/group.component';
import { RowComponent } from './components/layout/row.component';
import { ColComponent } from './components/layout/col.component';
import { ChartTempetureComponent } from './components/chart-tempeture.component';
import { LeafletComponent } from './components/leaflet/leaflet.component';
import { PadDirective } from './directives/pad.directive';
import { MarginDirective } from './directives/margin.directive';
import { CacchinaDirective } from './directives/cacchina.directive';
import { ThemeDirective } from './directives/theme.directive';
import { IsLoggedDirective } from './directives/is-logged.directive';
import { IfRoleIsDirective } from './directives/if-role-is.directive';
import { UrlDirective } from './directives/url.directive';
import { StopPropagationDirective } from './directives/stop-propagation.directive';
import { MyRouterLinkActiveDirective } from './directives/my-router-link-active.directive';
import { IfSigninDirective } from './directives/if-signin.directive';
import { LoaderDirective } from './directives/loader.directive';
import { LoaderCompoDirective } from './directives/loader-compo.directive';
import { CounterService } from './counter.service';

@NgModule({
  declarations: [
    WeatherComponent,
    ChartComponent,
    ListComponent,
    ListItemComponent,
    AccordionComponent,
    GroupComponent,
    RowComponent,
    ColComponent,
    ChartTempetureComponent,
    PadDirective,
    MarginDirective,
    CacchinaDirective,
    ThemeDirective,
    IsLoggedDirective,
    IfRoleIsDirective,
    UrlDirective,
    StopPropagationDirective,
    MyRouterLinkActiveDirective,
    IfSigninDirective,
    LoaderDirective,
    LoaderCompoDirective,
  ],
  imports: [
    CommonModule,
    PanelModule,
  ],
  exports: [
    WeatherComponent,
    ChartComponent,
    PanelModule,
    ListComponent,
    ListItemComponent,
    AccordionComponent,
    GroupComponent,
    RowComponent,
    ColComponent,
    ChartTempetureComponent,
    PadDirective,
    MarginDirective,
    CacchinaDirective,
    ThemeDirective,
    IsLoggedDirective,
    IfRoleIsDirective,
    UrlDirective,
    StopPropagationDirective,
    MyRouterLinkActiveDirective,
    IfSigninDirective,
    LoaderDirective,
    LoaderCompoDirective,
  ],

})
export class SharedModule {
  /*static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [
        CounterService
      ]
    }
  }*/
}
