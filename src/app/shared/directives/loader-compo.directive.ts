import { ComponentRef, Directive, Input, Type, ViewContainerRef } from '@angular/core';
import { PanelComponent } from '../components/panel/panel.component';
import { ListComponent } from '../components/list.component';
import { ChartTempetureComponent } from '../components/chart-tempeture.component';
import { LeafletComponent } from '../components/leaflet/leaflet.component';

const COMPONENTS: { [key: string]: Type<any> } = {
  panel: PanelComponent,
  list: ListComponent,
  'temp-chart': ChartTempetureComponent,
  'leaflet': LeafletComponent
}

@Directive({
  selector: '[teLoaderCompo]'
})
export class LoaderCompoDirective {
  @Input('teLoaderCompo') cfg!: { type: string, data: any };

  constructor(private view: ViewContainerRef) {}

  ngOnInit() {
    try {
      const compo: ComponentRef<any> = this.view.createComponent(COMPONENTS[this.cfg.type])
      for (let key in this.cfg.data) {
        compo.instance[key] = this.cfg.data[key]
      }
    } catch(e) {
      throw new Error('this compo is not supported:' + this.cfg.type)
    }
  }

}
