import { Directive, HostBinding } from '@angular/core';
import { ThemeService } from '../../core/theme.service';

@Directive({
  selector: '[teTheme]'
})
export class ThemeDirective {
  @HostBinding() get class() {
    return 'bg-' + this.themeSrv.value
  }

  constructor(private themeSrv: ThemeService) { }

}
