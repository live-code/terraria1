import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[teUrl]'
})
export class UrlDirective {
  @HostBinding('style.cursor') cursor = 'pointer'
  @HostBinding('style.color') get color() {
    return this.col
  }
  col = 'orange'

  @Input() teUrl: string = '';

  @HostListener('click')
  clickMe() {
    window.open(this.teUrl)
  }

  @HostListener('mouseover')
  mouseOver() {
    this.col = 'red'
  }

  @HostListener('mouseout')
  mouseOut() {
    this.col = 'orange'
  }


}
