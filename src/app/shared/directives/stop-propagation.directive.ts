import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[teStopPropagation]'
})
export class StopPropagationDirective {

  @HostListener('click', ['$event'])
  clickMe(event: MouseEvent) {
    event.stopPropagation();
  }

}
