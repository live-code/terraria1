import { Directive, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../core/auth.service';
import { distinctUntilChanged, filter, take } from 'rxjs';

@Directive({
  selector: '[teIfSignin]'
})
export class IfSigninDirective {
  constructor(
    private template: TemplateRef<any>,
    private view: ViewContainerRef,
    private authService: AuthService
  ) {
    authService.isLogged$
     .subscribe(value => {
       view.clear();
       if (value) {
         view.createEmbeddedView(this.template)
       }
     })

  }

}
