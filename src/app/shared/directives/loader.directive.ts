import { Directive, HostListener, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[teLoader]'
})
export class LoaderDirective {
  @Input() teLoader!: TemplateRef<any>

  constructor(
    private view: ViewContainerRef
  ) {
  }

  @HostListener('mouseover')
  showMenu() {
    this.view.createEmbeddedView(this.teLoader)
  }
  @HostListener('mouseout')
  hideMenu() {
    this.view.clear()
  }

}
