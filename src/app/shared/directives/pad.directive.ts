import { Directive, HostBinding, Input, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[tePad]'
})
export class PadDirective {
  @Input('tePad') paddingValue: 'p-0' | 'p-1' | 'p-5' = 'p-0';

  @HostBinding() get className() {
    return this.paddingValue
  }

  constructor() {
    console.log('pad')
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('changes', changes)
  }
}
