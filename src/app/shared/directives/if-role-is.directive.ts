import { Directive, HostBinding, Input } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Directive({
  selector: '[teIfRoleIs]'
})
export class IfRoleIsDirective {
  @Input() teIfRoleIs: string | undefined;

  @HostBinding() get hidden() {
    return !(this.auth.role === this.teIfRoleIs)
  }

  constructor(private auth: AuthService) { }
}
