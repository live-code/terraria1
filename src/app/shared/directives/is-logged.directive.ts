import { Directive, HostBinding } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Directive({
  selector: '[teIsLogged]'
})
export class IsLoggedDirective {
  @HostBinding() get hidden() {
    return !this.auth.isLogged
  }

  constructor(private auth: AuthService) { }


}
