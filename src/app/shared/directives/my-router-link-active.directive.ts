import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Directive({
  selector: '[teMyRouterLinkActive]'
})
export class MyRouterLinkActiveDirective {
  @Input() teMyRouterLinkActive!: string;

  constructor(
    private router: Router,
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2,
  ) {
    const requiredUrl = this.el.nativeElement.getAttribute('routerLink');
    router.events
      .subscribe(ev => {
        if (ev instanceof NavigationEnd) {
          if (requiredUrl && ev.url.includes(requiredUrl) ) {
            this.renderer.addClass(el.nativeElement, this.teMyRouterLinkActive)
          } else {
            this.renderer.removeClass(el.nativeElement, this.teMyRouterLinkActive)
          }
        }
      })

  }

}
