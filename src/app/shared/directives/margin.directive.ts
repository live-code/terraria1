import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[teMargin]'
})
export class MarginDirective {
  @Input() set teMargin(val: 1 | 2 | 3) {
    this.renderer.setStyle(
      this.elementRef.nativeElement,
      'margin',
      `${10 * val}px`
    )
    this.renderer.addClass(this.elementRef.nativeElement, 'bg-info')
    this.renderer.setAttribute(this.elementRef.nativeElement, 'pippo', '123')
    this.renderer.removeAttribute(this.elementRef.nativeElement, 'pippo')
  }

  @Input() set color(val: string) {
    this.renderer.setStyle(
      this.elementRef.nativeElement,
      'color',
      val
    )
  }

  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2
  ) {
    console.log('margin')
  }

}
