import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLogged = true;
  role = 'admin';
  // o--------o-----o-----o---o-->
  isLogged$ = new BehaviorSubject(false)

  constructor() {
    setTimeout(() => {
      this.login();
    }, 1000)
    setTimeout(() => {
      this.login();
    }, 2000)
  }

  login() {
    this.isLogged = true;
    this.isLogged$.next(true);
  }

  logout() {
    this.isLogged$.next(false);
  }
}
