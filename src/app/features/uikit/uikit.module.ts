import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UikitRoutingModule } from './uikit-routing.module';
import { UikitComponent } from './uikit.component';
import { SharedModule } from '../../shared/shared.module';
import { LeafletModule } from '../../shared/components/leaflet/leaflet.module';


@NgModule({
  declarations: [
    UikitComponent
  ],
  imports: [
    CommonModule,
    UikitRoutingModule,
    SharedModule,
    LeafletModule
  ]
})
export class UikitModule {
}
