import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'te-uikit',
  template: `
    
    <h1>Leaflet</h1>
    <te-leaflet [coords]="mapConfig.coords" 
                [zoom]="mapConfig.zoom"
                [markers]="mapConfig.markers"
    ></te-leaflet>
    
    <button (click)="mapConfig.coords = [11, 11]">Pos 1</button>
    <button (click)="mapConfig.coords = [22, 33]">Pos 2</button>
    <button (click)="mapConfig.zoom = mapConfig.zoom - 1">-</button>
    <button (click)="mapConfig.zoom = mapConfig.zoom + 1">+</button>
    
    <h1>HighChart</h1>
    <button
      (click)="data = [10, 15, 12]; chartType = 'bar'"
    >Update Data and type</button>
    
    <button
      (click)="chartType = 'spline'"
    >Update Type</button>
    
    <te-chart-tempeture
      [temperatures]="data"
      [type]="chartType"
      (itemClick)="doSomething($event)"
    ></te-chart-tempeture>
    <!--<te-chart-tempeture [temperatures]="[2,5,6]"></te-chart-tempeture>-->
    
  `,
  /*styleUrls: [
    '../../../../node_modules/leaflet.markercluster/dist/MarkerCluster.css',
    '../../../../node_modules/leaflet.markercluster/dist/MarkerCluster.Default.css'
  ]*/
})
export class UikitComponent  {
  data = [22, 33, 12]
  chartType: any = 'line';
  mapConfig: any = {
    zoom: 5,
    coords: [43, 13],
    markers: [[43,13], [43, 12], [42,11]]
  }


  doSomething(temp: number) {
    console.log(temp)
  }
}
