import { Component, ElementRef, Renderer2 } from '@angular/core';
import { Page, PAGES } from './model/page';
import { CounterService } from '../../shared/counter.service';

@Component({
  selector: 'te-login',
  template: `
    <h1 (click)="counter.inc()">{{counter.value}}</h1>

    <h1>Login</h1>
    <router-outlet></router-outlet>
    <hr>
    <button routerLink="registration" routerLinkActive="active">regi</button>
    <button routerLink="/login/lost" routerLinkActive="active">lost</button>
  `,
  styles: [
  ]
})
export class LoginComponent {
  page: Page = PAGES.SIGNIN;
  PAGES = PAGES;

  constructor(public counter: CounterService) {
  }
  changePage(page: Page) {
    this.page = page;
  }
}
