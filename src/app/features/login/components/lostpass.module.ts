import { NgModule } from '@angular/core';
import { LostpassComponent } from './lostpass.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    LostpassComponent
  ],
  exports: [
    LostpassComponent
  ],
  imports: [
    RouterModule.forChild([
      { path: '', component: LostpassComponent }
    ])
  ]
})
export class LostpassModule {

}
