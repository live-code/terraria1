import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login.component';
import { RegistrationComponent } from './components/registration.component';
import { SigninComponent } from './components/signin.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    children: [
      { path: 'signin', component: SigninComponent },
      { path: 'lost', loadChildren: () => import('./components/lostpass.module').then(m => m.LostpassModule) },
      { path: 'registration', component: RegistrationComponent },
      { path: '', redirectTo: 'signin'}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
