export const PAGES = {
  SIGNIN: 'signin',
  REGISTRATION: 'registration',
  LOST: 'lost',
}

export type Page = typeof PAGES.SIGNIN | 'registration' | 'lost'
