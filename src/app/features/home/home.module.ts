import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { NewsComponent } from './modules/news.component';
import { CarouselComponent } from './modules/carousel.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { HomeRoutingModule } from './home-routing.module';
import { FeaturesModule } from './modules/features/features.module';

@NgModule({
  declarations: [
    HomeComponent,
    NewsComponent,
    CarouselComponent,
  ],
  imports: [
    HomeRoutingModule,
    FormsModule,
    FeaturesModule,
    SharedModule,
  ]
})
export class HomeModule {

}
