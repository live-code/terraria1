import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { PanelComponent } from '../../shared/components/panel/panel.component';
import { CounterService } from '../../shared/counter.service';

@Component({
  selector: 'te-home',
  template: `
    
    <h1 (click)="counter.inc()">{{counter.value}}</h1>

    <h1>layout</h1>
  
    <te-row mq="lg">
      <te-col>left</te-col>
      <te-col>right</te-col>
    </te-row>
  
    
    <h1>weather</h1>
    <form #f="ngForm" (submit)="search()">
      <input #cityRef type="text" [ngModel]="label" name="city">
    </form>
    
    <te-weather [city]="label" [unit]="unit"></te-weather>
    <button (click)="unit = 'imperial'">Imperial</button>
    <button (click)="unit = 'metric'">Metric</button>
    <hr>
    
    <h1>Sub Modules</h1>
    <te-news></te-news>
    <te-carousel></te-carousel>
    <te-features></te-features>
    
    <h1>Panels</h1>
    <te-panel title="ciao1" icon="fa fa-times" >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad alias animi, asperiores corporis dolor dolore earum et ex facere facilis ipsum nam pariatur placeat quas rem sapiente sunt tempora totam.
    </te-panel>   
    
    <te-panel #panel title="ciao2" icon="fa fa-times" >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad alias animi, asperiores corporis dolor dolore earum et ex facere facilis ipsum nam pariatur placeat quas rem sapiente sunt tempora totam.
    </te-panel>
    
    <te-panel #panel title="ciao3" icon="fa fa-times" >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad alias animi, asperiores corporis dolor dolore earum et ex facere facilis ipsum nam pariatur placeat quas rem sapiente sunt tempora totam.
    </te-panel>
    <button (click)="closeAll()">close all</button>

    <hr>
    
    <h1>accordion</h1>
    <te-accordion>
      <te-group title="1">
        <input type="text">
      </te-group>
      <te-group title="2">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci amet dolor doloremque facere itaque iusto mollitia porro quasi voluptatibus voluptatum? Amet deleniti dicta doloribus enim neque nihil sequi sit ut?
      </te-group>
      <te-group title="3" >
        <button>click me</button>
      </te-group>
    </te-accordion>
    
  `
})
export class HomeComponent implements AfterViewInit {
  label: string | null = 'Palermo';
  unit: 'imperial' | 'metric' = 'metric';
  @ViewChild('cityRef') cityRef!: ElementRef<HTMLInputElement>;
  @ViewChild(PanelComponent) panel!: PanelComponent;
  @ViewChildren(PanelComponent) panels!: QueryList<PanelComponent>;

  panel1open = true;

  constructor(private cd: ChangeDetectorRef, public counter: CounterService) {
  }
  ngAfterViewInit(): void {
    //
    // const panels = this.panels.toArray();
    // panels.forEach(p => {
    //   p.isOpen = false
    // })
    //
    // this.cd.detectChanges()
  }

  closeAll() {
    const panels = this.panels.toArray();
    panels.forEach(p => {
     p.isOpen = false
    })

    /*this.cityRef.nativeElement.focus();
    console.log(this.panelComponent.isOpen)
    setTimeout(() => {
      this.panelComponent.isOpen = false
      this.panelComponent.iconClick
        .subscribe(() => {
          console.log('icon click')
        })
    }, 2000)*/
  }

  search() {
    console.log(this.cityRef.nativeElement.value)
    // this.label = this.cityRef
  }
}
