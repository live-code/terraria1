import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { ListComponent } from '../../shared/components/list.component';
import { ThemeService } from '../../core/theme.service';

@Component({
  selector: 'te-users',
  template: `
    {{themeService.value}}
    <button (click)="visible = !visible">close panel </button>
    <te-panel 
      title="my widget" 
      icon="fa fa-google"
      headerCls="bg-success"
      [(isOpen)]="visible"
      (iconClick)="openUrl('http://www.google.com')"
    >
     <button>1</button>
     <button>2</button>
    </te-panel>
    
    <te-panel title="ciao"  [isOpen]="visible">
      bla bla
    </te-panel>
    
    <te-list 
      [items]="users" 
      labelField="name"
      icon="fa fa-trash"
      (iconClick)="delete($event)"
      [theme]="themeService.value"
    ></te-list>
    
    
  `,
})
export class UsersComponent  {
  visible = false;
  users: User[] | null = null

  constructor(private http: HttpClient, public themeService: ThemeService) {
    http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => {
        this.users = res;
      })

    setTimeout(() => {
      this.themeService.value = 'light'
    }, 2000)

  }

  delete(user: User) {
    this.http.delete('https://jsonplaceholder.typicode.com/users/' + user.id)
      .subscribe(() => {
        if (this.users) {
          // const index = this.users.findIndex(u => u.id === user.id)
          // this.users.splice(index, 1)
          this.users = this.users.filter(u => u.id !== user.id);
        }
      })
    console.log('delete', user.id)
  }

  openUrl(url: string) {
    window.open(url)
  }

  showAlert(text: string) {
    window.alert(text)
  }

}
