import { Component, ElementRef, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'te-uikit-directives',
  template: `

    <te-widgets
      [cfg]="json"
     >
    </te-widgets>
<!--
    <ng-container
      *ngFor="let cfg of json"
      [teLoaderCompo]="cfg">
    </ng-container>-->
    
    <hr>
<!--
    <ng-container *ngFor="let cfg of json">
      <ng-container [ngSwitch]="cfg.type">
        <te-panel [title]="cfg.data.title || ''" *ngSwitchCase="'panel'"></te-panel>
        <te-list [items]="cfg.data.items"  *ngSwitchCase="'list'"></te-list>
        <te-leaflet [coords]="cfg.data.coords" *ngSwitchCase="'leaflet'"></te-leaflet>
      </ng-container>
    </ng-container>

    -->

    <h1 *teIfSignin>Sono loggato</h1>
    <hr>
    
    <div [teLoader]="menu">APRI MENU</div>
   
    
    <span teUrl="http://www.google.com">visit it</span>. è bellissimo!
    
    <div (click)="parent()">
      <div (click)="child()" teStopPropagation>
        sTOP pRoppagaTIOn
      </div>
    </div>
    <hr>
    
    <div teIsLogged>sono loggato</div>
    <div teIfRoleIs="admin">sei admin. percio lo vedi!</div>
    
    <h1 teTheme>Pad</h1>
    <input type="text" [(ngModel)]="padValue">
    <span [tePad]="padValue">
      ciao a tutti cacc
    </span>

    <h1>Margin</h1>
    <span [teMargin]="3" color="white">
      ciao a tutti cacc
    </span>

    <ng-template #menu>
      <div>menu item</div>
      <div>menu item</div>
      <div>menu item</div>
    </ng-template>
   
  `,
  styles: [
  ]
})
export class UikitDirectivesComponent {

  json: any = [

    {
      type: 'list', data: { items: [{ title: 'a'}, {title: 'b'}]}
    },
    {
      type: 'temp-chart', data: { temperatures: [10,20,15]}
    },
    {
      type: 'leaflet', data: { coords: [42,13]}
    },
    {
      type: 'panel', data: { title: 'ciao', icon: 'fa fa-times'}
    },
  ]
  padValue: any = 'p-1'
  @ViewChild('error') errPanel!: TemplateRef<any>


  dosomething() {

  }

  child() {
    alert('child')
  }

  parent() {
    alert('palert')
  }
}


// document.getElementById('x').style.fontSize = '20px'
