import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UikitDirectivesRoutingModule } from './uikit-directives-routing.module';
import { UikitDirectivesComponent } from './uikit-directives.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { LeafletModule } from '../../shared/components/leaflet/leaflet.module';
import { WidgetsComponent } from './components/widgets.component';


@NgModule({
  declarations: [
    UikitDirectivesComponent,
    WidgetsComponent
  ],
  imports: [
    CommonModule,
    UikitDirectivesRoutingModule,
    SharedModule,
    FormsModule,
    LeafletModule
  ]
})
export class UikitDirectivesModule { }
