import { Component, ComponentRef, Input, OnInit, Type, ViewContainerRef } from '@angular/core';
import { PanelComponent } from '../../../shared/components/panel/panel.component';
import { ListComponent } from '../../../shared/components/list.component';
import { ChartTempetureComponent } from '../../../shared/components/chart-tempeture.component';
import { LeafletComponent } from '../../../shared/components/leaflet/leaflet.component';

const COMPONENTS: { [key: string]: Type<any> } = {
  panel: PanelComponent,
  list: ListComponent,
  'temp-chart': ChartTempetureComponent,
  'leaflet': LeafletComponent
}

@Component({
  selector: 'te-widgets',
  template: `
    <div>Dashboard</div>
    xxxx
    <div>Footer</div>
    
    <hr>
  `,
})
export class WidgetsComponent implements OnInit {
  @Input() cfg!: { type: string, data: any }[];

  constructor(private view: ViewContainerRef) {}

  ngOnInit() {
    for(let item of this.cfg) {
      try {
        const compo: ComponentRef<any> = this.view.createComponent(COMPONENTS[item.type])
        for (let key in item.data) {
          compo.instance[key] = item.data[key]
        }
      } catch(e) {
        throw new Error('this compo is not supported:' + item.type)
      }
    }

  }

}
