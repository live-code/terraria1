'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">terraria1 documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-a3dcc36c51c58442f06c760e1368ef2bfea5d1ffdb8bfd4f4c2e50a211699dba98be327ee30cfee3f7e8ae4b1c211880b2ee898852d7ac456e5fa908157a1434"' : 'data-target="#xs-components-links-module-AppModule-a3dcc36c51c58442f06c760e1368ef2bfea5d1ffdb8bfd4f4c2e50a211699dba98be327ee30cfee3f7e8ae4b1c211880b2ee898852d7ac456e5fa908157a1434"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-a3dcc36c51c58442f06c760e1368ef2bfea5d1ffdb8bfd4f4c2e50a211699dba98be327ee30cfee3f7e8ae4b1c211880b2ee898852d7ac456e5fa908157a1434"' :
                                            'id="xs-components-links-module-AppModule-a3dcc36c51c58442f06c760e1368ef2bfea5d1ffdb8bfd4f4c2e50a211699dba98be327ee30cfee3f7e8ae4b1c211880b2ee898852d7ac456e5fa908157a1434"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/FeaturesModule.html" data-type="entity-link" >FeaturesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-FeaturesModule-ec0b5f9d2f2749820b43a505e8bad240fbe2f0eaab095e05f5cb7abb935ce04c3207d9f098752d4edf2b87180a22fad4ed5b0b6786971a45a945e5b3b566d9ac"' : 'data-target="#xs-components-links-module-FeaturesModule-ec0b5f9d2f2749820b43a505e8bad240fbe2f0eaab095e05f5cb7abb935ce04c3207d9f098752d4edf2b87180a22fad4ed5b0b6786971a45a945e5b3b566d9ac"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-FeaturesModule-ec0b5f9d2f2749820b43a505e8bad240fbe2f0eaab095e05f5cb7abb935ce04c3207d9f098752d4edf2b87180a22fad4ed5b0b6786971a45a945e5b3b566d9ac"' :
                                            'id="xs-components-links-module-FeaturesModule-ec0b5f9d2f2749820b43a505e8bad240fbe2f0eaab095e05f5cb7abb935ce04c3207d9f098752d4edf2b87180a22fad4ed5b0b6786971a45a945e5b3b566d9ac"' }>
                                            <li class="link">
                                                <a href="components/FeaturesComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FeaturesComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomeModule.html" data-type="entity-link" >HomeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HomeModule-a827bcdc03684498cba3ce8414e1ed46b4945b1128c16432e3592ea1c66a3d3c3fb67950b20fa19e68e4902711a920a09e78b6c6c9c9acbc3577400829b1393e"' : 'data-target="#xs-components-links-module-HomeModule-a827bcdc03684498cba3ce8414e1ed46b4945b1128c16432e3592ea1c66a3d3c3fb67950b20fa19e68e4902711a920a09e78b6c6c9c9acbc3577400829b1393e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HomeModule-a827bcdc03684498cba3ce8414e1ed46b4945b1128c16432e3592ea1c66a3d3c3fb67950b20fa19e68e4902711a920a09e78b6c6c9c9acbc3577400829b1393e"' :
                                            'id="xs-components-links-module-HomeModule-a827bcdc03684498cba3ce8414e1ed46b4945b1128c16432e3592ea1c66a3d3c3fb67950b20fa19e68e4902711a920a09e78b6c6c9c9acbc3577400829b1393e"' }>
                                            <li class="link">
                                                <a href="components/CarouselComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CarouselComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HomeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HomeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NewsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NewsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomeRoutingModule.html" data-type="entity-link" >HomeRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/LoginModule.html" data-type="entity-link" >LoginModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LoginModule-33b9d92880ddcb5813b96913c9f08b89b927f0d541a8c2294d20e2585d56ebb2d61373cf0f43e1d7c724365f8cb055c3451f3a83cb47a2b60be31637454cc3f9"' : 'data-target="#xs-components-links-module-LoginModule-33b9d92880ddcb5813b96913c9f08b89b927f0d541a8c2294d20e2585d56ebb2d61373cf0f43e1d7c724365f8cb055c3451f3a83cb47a2b60be31637454cc3f9"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoginModule-33b9d92880ddcb5813b96913c9f08b89b927f0d541a8c2294d20e2585d56ebb2d61373cf0f43e1d7c724365f8cb055c3451f3a83cb47a2b60be31637454cc3f9"' :
                                            'id="xs-components-links-module-LoginModule-33b9d92880ddcb5813b96913c9f08b89b927f0d541a8c2294d20e2585d56ebb2d61373cf0f43e1d7c724365f8cb055c3451f3a83cb47a2b60be31637454cc3f9"' }>
                                            <li class="link">
                                                <a href="components/LoginComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginRoutingModule.html" data-type="entity-link" >LoginRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/LostpassModule.html" data-type="entity-link" >LostpassModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LostpassModule-d4f0ede81acbf0c74cc384914553ca0276f2e06a7f3eb6b89991e20559f3eab47b564f7b4fafffc264676ae5dc191ca87d8f7bbf42ec717a901d74397a6fe837"' : 'data-target="#xs-components-links-module-LostpassModule-d4f0ede81acbf0c74cc384914553ca0276f2e06a7f3eb6b89991e20559f3eab47b564f7b4fafffc264676ae5dc191ca87d8f7bbf42ec717a901d74397a6fe837"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LostpassModule-d4f0ede81acbf0c74cc384914553ca0276f2e06a7f3eb6b89991e20559f3eab47b564f7b4fafffc264676ae5dc191ca87d8f7bbf42ec717a901d74397a6fe837"' :
                                            'id="xs-components-links-module-LostpassModule-d4f0ede81acbf0c74cc384914553ca0276f2e06a7f3eb6b89991e20559f3eab47b564f7b4fafffc264676ae5dc191ca87d8f7bbf42ec717a901d74397a6fe837"' }>
                                            <li class="link">
                                                <a href="components/LostpassComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LostpassComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link" >SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-27cff38d9ede80740fdd108b0e974e65d169fa69118f47835e07edabd46065444c044323c8a54335748e9f80b92146c1691cb62f26f18512f7770440c030cb67"' : 'data-target="#xs-components-links-module-SharedModule-27cff38d9ede80740fdd108b0e974e65d169fa69118f47835e07edabd46065444c044323c8a54335748e9f80b92146c1691cb62f26f18512f7770440c030cb67"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-27cff38d9ede80740fdd108b0e974e65d169fa69118f47835e07edabd46065444c044323c8a54335748e9f80b92146c1691cb62f26f18512f7770440c030cb67"' :
                                            'id="xs-components-links-module-SharedModule-27cff38d9ede80740fdd108b0e974e65d169fa69118f47835e07edabd46065444c044323c8a54335748e9f80b92146c1691cb62f26f18512f7770440c030cb67"' }>
                                            <li class="link">
                                                <a href="components/ChartComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ChartComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PanelComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PanelComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WeatherComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WeatherComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersModule.html" data-type="entity-link" >UsersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UsersModule-f400396150aa95b743a5631d110c70c35940c34d33bc6a077747d613f74f730cd914af561b666759a40239a2174471cd0e2c37fa4316b8dd89d0e1f4074984de"' : 'data-target="#xs-components-links-module-UsersModule-f400396150aa95b743a5631d110c70c35940c34d33bc6a077747d613f74f730cd914af561b666759a40239a2174471cd0e2c37fa4316b8dd89d0e1f4074984de"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UsersModule-f400396150aa95b743a5631d110c70c35940c34d33bc6a077747d613f74f730cd914af561b666759a40239a2174471cd0e2c37fa4316b8dd89d0e1f4074984de"' :
                                            'id="xs-components-links-module-UsersModule-f400396150aa95b743a5631d110c70c35940c34d33bc6a077747d613f74f730cd914af561b666759a40239a2174471cd0e2c37fa4316b8dd89d0e1f4074984de"' }>
                                            <li class="link">
                                                <a href="components/UsersComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UsersComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersRoutingModule.html" data-type="entity-link" >UsersRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#components-links"' :
                            'data-target="#xs-components-links"' }>
                            <span class="icon ion-md-cog"></span>
                            <span>Components</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="components-links"' : 'id="xs-components-links"' }>
                            <li class="link">
                                <a href="components/ContactsComponent.html" data-type="entity-link" >ContactsComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/RegistrationComponent.html" data-type="entity-link" >RegistrationComponent</a>
                            </li>
                            <li class="link">
                                <a href="components/SigninComponent.html" data-type="entity-link" >SigninComponent</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});